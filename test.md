---
marp: true
theme: gaia
header: "**thermo-fluid lab.**"
footer: "by taka"
---
<style>
    section {
        font-family: Avenir Next Condensed
    }

    :root {
        --color-background: #101010;
        --color-foreground: #FFFFFF;
    }
</style>
<!-- $size: 16:9 -->
# Sample presentation slide by using Marp (VS Code)
---

# Equation sample

$$ 
    \sigma = E \epsilon\\
$$

$$
    \frac{\partial {\bf u}}{\partial t} + ({\bf u} \cdot \nabla ) {\bf u}
    = \frac{1}{\rho} \text{grad} p + \nu \nabla^2 {\bf u}
$$

---

![bg right:60% contrast:1.0 brightnes:1.0](benjamin.jpg)
![bg right:60% contrast:1.0 brightnes:1.0](mak.jpg)
### Figure sample 0

---

### Figure sample 1
![bg left:40% contrast:0.2 brightnes:1.0](benjamin.jpg)

- figures on the left side of a slide (with contrast adjustment)

---

### Figure sample 2
![bg](benjamin.jpg)
![bg](mak.jpg)

- two figures on whole of a slide

---

### Figure sample 3

![bg right](benjamin.jpg)

- simple insertion of a figure on a slide

---

### Figure sample 4
![bg blur right](benjamin.jpg)
- add blur effect on a figure
